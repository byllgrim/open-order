import React from 'react'

import TitleBar from './TitleBar'
import OrderList from './OrderList'

export default function CustomerPickUpScreen() {
  return (
    <div>
      <TitleBar/>
      <OrderList/>
    </div>
  )
}
