import React from 'react'
import ReactDOM from 'react-dom'

import CustomerPickUpScreen from './CustomerPickUpScreen'

import '../src/style.css'


const rootElement = document.getElementById('root')

ReactDOM.render(<CustomerPickUpScreen/>, rootElement)
