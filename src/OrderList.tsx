import React from 'react'

interface OrderRow {
  order_number:  number;
  customer_name: string;
  status:        string;
  ready_in:      string;
}

import orderDataUntyped from './test_data.json'
const orderData: OrderRow[] = orderDataUntyped

function orderRowToTableRow(orderRow: OrderRow) {
  return (
    <tr>
      <td>{orderRow.order_number}</td>
      <td>{orderRow.customer_name}</td>
      <td>{orderRow.status}</td>
      <td>{orderRow.ready_in}</td>
    </tr>
  )
}

export default function OrderList() {
  return (
    <table className="order-list">
      <tr>
        <th>Order</th>
        <th>Name</th>
        <th>Status</th>
        <th>Ready In</th>
      </tr>
      { orderData.map(orderRowToTableRow) }
    </table>
  );
}
