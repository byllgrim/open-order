import React from 'react'

export default function TitleBar() {
  return (
    <h1 className="title-bar">
      Now Serving
    </h1>
  );
}
