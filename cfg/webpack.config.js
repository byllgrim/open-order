const HtmlWebpackPlugin = require('html-webpack-plugin')

module.exports = {
  entry: './build/index.js',
  mode: "development",
  plugins: [
    new HtmlWebpackPlugin({
      template: "./src/index.html"
    })
  ],
  module: {
    rules: [
      {
        test: /\.css$/i,
        use: ["style-loader", "css-loader"]
      }
    ]
  }
}
