default:
	@echo Read the Makefile

compile: comp
comp:
	npm install
	tsc  --project ./cfg/tsconfig.json

run:
	npm start
